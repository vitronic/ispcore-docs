---
title: "CRM"
weight: 2
type: 'docs'
---

# CRM en CixCore.

El CRM de CixCore permite mantener y registrar historicamente la relación entre el cliente y la empresa, 
de tal manera que se pueda en cualquier momento saber las actividades realizadas sobre, o con un cliente, 
también le permitira mantener un catalogo personal de los clientes e interactuar con ellos, es importante 
aclarar que en el CRM de CixCore las cuentas de clientes registradas solo pueden ser vistas 
por el vendedor que las registró.

## ¿Cómo puedo registrar un nuevo cliente?.

<a href="/ispcore-docs/images/crm-accounts-new-button.png" target="_new">![Dirijase al menú "CRM > Cuentas" a continuación pulse el "botón +" en la esquina superior derecha](/ispcore-docs/images/crm-accounts-new-button.png "Dirijase al menú “CRM > Cuentas” a continuación pulse el “botón +” en la esquina superior derecha")</a>

Dirijase al menú __CRM > Cuentas__ a continuación pulse el __botón +__ en la esquina superior derecha,
esto le permitira acceder al formulario de registros de una nueva cuenta cliente,
a continuación detallaremos los campos del formulario:

<a href="/ispcore-docs/images/crm-accounts-new-form.png" target="_new">![Esto le permitira acceder al formulario de registros de una nueva cuenta cliente](/ispcore-docs/images/crm-accounts-new-form.png "Esto le permitira acceder al formulario de registros de una nueva cuenta cliente")</a>

* __Número de identificación__: Se trata de una doble entrada donde se debe definir el tipo de documento 
y el código, en cuanto al tipo dispone en el menú despegable de siete opciones:

    1. __Cédula__: La cédula de la razón social.
    2. __Cédula E__: La cédula en el caso de que la razón social sea extranjera.
    3. __Natural__: R.I.F de una persona natural.
    4. __Juridico__: R.I.F de una persona o ente juridico.
    5. __Extranjero__: R.I.F de una persona natural extranjero.
    6. __Comuna__: R.I.F de una comuna.
    7. __R.I.F__: para entes del gobierno.
    
En cuanto al código se trata del R.I.F o de la cédula de la razón social sujeta al formato descrito anteriormente 

* __Nombre completo__: Ingrese el nombre completo de la razón social.
* __Número telefonico__: Se trata de una doble entrada donde se puede ingresar el número telefonico y el recurso, 
puede ingresar tantos numeros telefonicos como considere necesario.
* __Dirección del correo eléctronico__: Ingrese la dirección del correo eléctronico del contacto, puede 
ingresar tantos correos eléctronicos como considere necesario.
* __Dirección completa__: Introduzca la dirección fiscal exacta, se utilizara por defecto con propositos de facturación.
* __Grupos__: Seleccione del menú tantos grupos como considere necesario para el registro de la cuenta.
* __Propietarios__: Seleccione de la lista los propietarios que tendrán acceso para gestionar la cuenta,
normalmente seleccionese a sí mismo.
* __Seleccione el estado de la cuenta__: Este apartado le permitira seleccionar el estado de la cuenta, 
para nuevos clientes en etapa de negociación se sugiere utilizar el estado __No definido__ o __Pendiente__, 
una vez que el cliente haya contratado algún servicio se deberá cambiar este estado a __Cliente__, es importante
aclarar que solo se puede facturar a una cuenta con estado de __Cliente__.
* __Seleccione moneda__: Seleccione a continuación la moneda con la que se realizara la facturación 
de la cuenta.
* __Tratamiento de la cuenta__: Seleccione de la lista el modo en el que será tratado la razón social.
* __Cumpleaños__: Opcionalmente seleccione el cumpleaños o aniversario de la cuenta 
(Se utilizara con propositos de marketing).
* __Envíar anuncios__: Defina del menú si desea o no envíar anuncios a la dirección de la cuenta
(Se utilizara con propositos de marketing).
* __Origen de la cuenta__: Seleccione del menú el origen de la cuenta 
(Para ser usado con propositos estadisticos).
* __Retención de impuestos__: En el caso de que la razón social sea un agente de retención de impuestos, 
habilitar esta opción y luego habilitar los impuestos que esta cuenta retiene.
<a href="/ispcore-docs/images/crm-accounts-new-form-retencion-impuestos.png" target="_new">
![Retención de impuestos](/ispcore-docs/images/crm-accounts-new-form-retencion-impuestos.png "Retención de impuestos")
</a>
* __Adjuntar imagen__: Opcionalmente usted puede adjuntar una imagen que servira para identificar 
visualmente a la cuenta que está registrando.

Finalmente el __botón envíar__ le permitira guardar los datos del formulario.

## ¿Cómo puedo realizar una cotización?.

Dirijase al menú __CRM > Cotizaciones__ a continuación pulse el __botón +__ en la esquina superior derecha 
esto le permitira acceder al formulario de registro de una nueva cotización, a continuación 
detallaremos los campos del formulario:
<a href="/ispcore-docs/images/cotizacion.png" target="_new">![¿Cómo puedo realizar una cotización?.](/ispcore-docs/images/cotizacion.png "¿Cómo puedo realizar una cotización?.")</a>

1. __Fecha de emisión__: Esta es la fecha en la que se emite el documento.
2. __Fecha de vencimiento__: Esta es la fecha en la que el documento caduca.
3. __Vendedor__: El nombre del responsable del documento (Por defecto es quien está registrando 
el documento).
4. __Forma de pago__: Seleccione entre efectivo o deposito bancario.
5. __Seleccione la cuenta__: Ingrese el nombre o la cédula de la razon social y a continuación seleccione 
la cuenta a la cual realizara la cotización.
6. __Seleccione un producto o servicio__: Ingrese el código o el nombre de un producto o servicio y 
a continuación seleccione, a continuación se le presentara un documento como el siguente 
(Colocar pantallaso aquí) 
puede seleccionar tantos productos o servicios como sea requerido, puede editar el precio de cada 
producto o servicio, así como también la cantidad o el descuento.
7. __Enviar por correo eléctronico__: Si desea enviar el documento por correo eléctronico seleccione 
el botón, defina el asunto y el cuerpo del correo eléctronico.

Finalmente para guardar la cotización pulse el botón __Guardar los cambios__

## Gestión de cuentas

Existen dos formas para ingresar al perfil de una cuenta y de ese modo gestionar la misma, una forma 
es ingresar el R.I.F/Cédula en el buscador principal, seleccionar la cuenta y pulsar en el botón perfil

<a href="/ispcore-docs/images/gestion-cuentas.png" target="_new">![Gestión de cuentas](/ispcore-docs/images/gestion-cuentas.png "Gestión de cuentas")</a>

la otra es simplemente ubicar la cuenta en el reporte principal y presionar el boton perfil en el lado 
derecho del reporte.

### ¿Cómo puedo registrar alguna nota sobre un cliente?

Una vez ingresado al perfil de una cuenta usted puede registrar una nota en la pestaña __Nota__
y pulsando el botón guardar.

<a href="/ispcore-docs/images/notas.png" target="_new">![¿Cómo puedo registrar alguna nota sobre un cliente?](/ispcore-docs/images/notas.png "¿Cómo puedo registrar alguna nota sobre un cliente?")</a>

### ¿Cómo puedo registrar una llamada telefonica de un cliente?

Dirijase a la pestaña "Llamadas" 

<a href="/ispcore-docs/images/llamada.png" target="_new">![¿Cómo puedo registrar una llamada telefonica de un cliente?](/ispcore-docs/images/llamada.png "¿Cómo puedo registrar una llamada telefonica de un cliente?")</a>

seleccione el numero telefonico, a continuación el tipo de llamada, luego el asunto de la llamada y 
el detalle de la llamada, finalmente defina el resultado de la llamada y guarde para realizar el 
registro, esta llamada quedara registrada con propositos historicos.

### ¿Cómo puedo enviarle un mensaje SMS a un cliente?

Dirijase a la pestaña __SMS__, seleccione el numero telefonico, luego seleccione una importancia y 
finalmente escriba el mensaje.

<a href="/ispcore-docs/images/enviar-sms.png" target="_new">![¿Cómo puedo enviarle un mensaje SMS a un cliente?](/ispcore-docs/images/enviar-sms.png "¿Cómo puedo enviarle un mensaje SMS a un cliente?")</a>

### ¿Cómo puedo enviarle un Email a un cliente?

Dirijase a la pestaña __Email__", seleccione uno o varios destinatarios, seleccione una prioridad del 
correo, ingrese un asunto y el cuerpo del correo electronico, finalmente pulse enviar E-mail.

<a href="/ispcore-docs/images/enviar-email.png" target="_new">![¿Cómo puedo enviarle un Email a un cliente?](/ispcore-docs/images/enviar-email.png "¿Cómo puedo enviarle un Email a un cliente?")</a>
